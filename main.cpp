//Download CImg.h and put it on the same folder.
//in Linux, compile with:
// g++ main.cpp -o imageViewer -lX11 -pthreads

#include <iostream>
#include "CImg.h"
#include "pyramid.hpp"

using namespace cimg_library;

int main(int argc, char* argv[]) {

  CImg<double> img1, img2, imgB;
  bool hasBlendingMask = false;

  if (argc < 2) { // Check the value of argc. If not enough parameters have been passed, inform user and exit.
    std::cout << "Usage is: \n <image 1 path> <image 2 path> <blending mask path> \n"<<
      "The blending image should be a black and white image \n"<<
      "Press any key to close... \n";
      std::cin.get();
      exit(0);
  } else {
    //try and open both files
    try {
      img1.assign(argv[1]);
      img2.assign(argv[2]);
      if(argc > 3) {
        imgB.assign(argv[3]);
        hasBlendingMask = true;
      }
    } catch (int e) {
      std::cout << "Unable to open files. Use -h for help\n";
      std::cin.get();
      exit(0);
    }

  }

  int levels = 3;

  std::cout<<"Building Gaussian Pyramids and converting to Laplacian"<<std::endl;
  //Pyramid 1
  ImagePyramid p1;
  p1.computeGaussianPyramid(img1.data(), levels, img1.width(), img1.height(), img1.spectrum());
  p1.convertToLaplacian();
  

  //Pyramid 2
  ImagePyramid p2;
  p2.computeGaussianPyramid(img2.data(), levels, img2.width(), img2.height(), img2.spectrum());
  p2.convertToLaplacian();
  

  //Blending pyramid
  ImagePyramid pb;
  if (hasBlendingMask) {
    pb.computeGaussianPyramidMask(imgB.data(), levels, imgB.width(), imgB.height());
  } else {
    pb.fillWithHalfMask(levels, img1.width(), img1.height(), (img1.width())/2);
  }


  //Saving pyramids
  /**
  for (int i = 0; i < levels; ++i)
  {
    char buffer1[10];
    sprintf(buffer1, "a%d.bmp", i);
    CImg<double> imgTemp1(p1.getLevelData(i), p1.getLevelWidth(i), p1.getLevelHeight(i), 1, p1.getSpectrum());
    imgTemp1.save_bmp(buffer1);
    
    char buffer2[10];
    sprintf(buffer2, "b%d.bmp", i);
    CImg<double> imgTemp2(p2.getLevelData(i), p2.getLevelWidth(i), p2.getLevelHeight(i), 1, p2.getSpectrum());
    imgTemp2.save_bmp(buffer2);

    char buffer3[10];
    sprintf(buffer3, "c%d.bmp", i);
    CImg<double> imgTemp3(pb.getLevelData(i), pb.getLevelWidth(i), pb.getLevelHeight(i), 1, pb.getSpectrum());
    imgTemp3.save_bmp(buffer3);
  }
  /**/

  std::cout<<"Blending Pyramids and collapsing"<<std::endl;
  //Blending pyramids together
  p1.blendWith(p2, pb);
  p1.collapsePyramid();

  std::cout<<"Done! Displaying..."<<std::endl;
  //Saving final result
  CImg<double> imgFinal(p1.getLevelData(0), p1.getLevelWidth(0), p1.getLevelHeight(0), 1, p1.getSpectrum());
  char final[9];
  sprintf(final, "final.bmp");
  imgFinal.save_bmp(final);


  //display final image
  /**/
  CImgDisplay img1_disp(imgFinal, "Image 1");
  while (!img1_disp.is_closed()) { 
  }
  /**/

  return 0;
}
