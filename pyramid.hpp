#include <cmath>
#include <iostream>

//Gaussian Pyramid Class
class ImagePyramid {
public:

	/**
	 * @brief Constructor
	 * @details Initializes variables
	 */
	ImagePyramid() {
		initialize();
	}

	/**
	 * @brief Fills pyramid's level0 with half black half white mask
	 * 
	 * @param nlev Number of pyramid levels
	 * @param w Width of mask
	 * @param h Height of mask
	 * @param halfw Where to divide the mask on x axis
	 * @return Returns true if succeeded
	 */
	bool fillWithHalfMask(int nlev, int w, int h, int halfw) {
		setNumberOfLevels(nlev);
		setDimensions(w, h, 1);
		createArrays();
		computeKernel();

		int pixel = 0;
		for (int x = 0; x < width; ++x)
		{
			for (int y = 0; y < height; ++y)
			{
				pixel = x + y*w;
				levels[0][pixel] = (x < halfw) ? 255.0 : 0.0; 
			}
		}

		privateComputeNonInterleaved();
		return true;
	}

	/**
	 * @brief Function called to compute Gaussian pyramid and store in-class using only
	 * @details Function called to compute Gaussian pyramid and store in-class using 
	 * only the first channel of the input image, to create B&W mask.
	 * 
	 * @param data Original image data array
	 * @param nlev Number of desired pyramid levels
	 * @param w Width of original image
	 * @param h Height of original image
	 * @return Returns true if function succeded
	 */
	bool computeGaussianPyramidMask(const double *data, int nlev, int w, int h) {
		computed = setNumberOfLevels(nlev) &&
			setDimensions(w, h, 1) &&
			createArraysAndFillNormalized(data) &&
			computeKernel() &&
			privateComputeNonInterleaved();
		return computed;
	}

	/**
	 * @brief Function called to compute Gaussian pyramid and store in-class
	 * @details Designed for non-interleaved images
	 * 
	 * @param data Original image data array
	 * @param dZero Gaussian Cutoff Frequency
	 * @param nlev Number of desired pyramid levels
	 * @param w Width of original image
	 * @param h Height of original image
	 * @param c Number of channels of original image
	 * @return Returns true if function succeded
	 */
	bool computeGaussianPyramid(const double *data, double dZero, int nlev, int w, int h, int c) {
		computed = setNumberOfLevels(nlev) &&
			setDimensions(w, h, c) &&
			createArraysAndFill(data) &&
			computeKernel(dZero) &&
			privateComputeNonInterleaved();
		return computed;
	}

	/**
	 * @brief Function called to compute Gaussian pyramid and store in-class
	 * @details Designed for non-interleaved images
	 * 
	 * @param data Original image data array
	 * @param nlev Number of desired pyramid levels
	 * @param w Width of original image
	 * @param h Height of original image
	 * @param c Number of channels of original image
	 * @return Returns true if function succeded
	 */
	bool computeGaussianPyramid(const double *data, int nlev, int w, int h, int c) {
		computed = setNumberOfLevels(nlev) &&
			setDimensions(w, h, c) &&
			createArraysAndFill(data) &&
			computeKernel() &&
			privateComputeNonInterleaved();
		return computed;
	}

	/**
	 * @brief Expand desired level of pyramid and save it on temp array (get it with getTempData())
	 * @details Designed for non-interleaved images
	 * 
	 * @param l Desired level (Eg. If 1, expands level 1 to size of level 0)
	 * @return Returns true if succeeded
	 */
	bool expandLevel (int l) {
		if ((l-1)<0 || (l-1)>=nlevels)
		{
			return false;
		}
		int loww = levelDimensions[l-1][0];
		int lowh = levelDimensions[l-1][1];

		double *t = new double[loww*lowh*channels];

		int highw = levelDimensions[l][0];
		int highh = levelDimensions[l][1];
		int nx = 0; int ny = 0;
		int lowpixel=0; int highpixel = 0;
		for (int c = 0; c < channels; ++c)
		{
			for (int y = 0; y < lowh; ++y)
			{
				for (int x = 0; x < loww; ++x)
				{
					nx = x/2;
					ny = y/2;
					nx = (nx >= highw) ? 2*highw-nx-1 : nx;
					ny = (ny >= highh) ? 2*highh-ny-1 : ny;
					lowpixel = x + y*loww + c*loww*lowh;
					highpixel = nx + ny*highw + c*highw*highh;
					t[lowpixel] = levels[l][highpixel];
				}
			}
		}

		/**/
		if (temp != 0)
		{
			delete [] temp;
		}
		temp = new double[loww*lowh*channels];
		double value = 0.0;
		for (int c = 0; c < channels; ++c)
		{
			for (int x = 0; x < loww; ++x)
			{
				for (int y = 0; y < lowh; ++y)
				{
					lowpixel = x + y*loww + c*lowh*loww;
					value = applyKernelTemp(t, x, y, c, loww, lowh, channels);
					truncate0_255(value);
					temp[lowpixel] = value;
				}
			}
		}

		delete [] t;
		/**/

		//printLevels();
		return true;
	}

	/**
	 * @brief Get pointer to temp array
	 * @return Pointer to double* temp
	 */
	double* getTempData () {
		return temp;
	}

	/**
	 * @brief Get image data for pyramid level
	 * @param l Desired pyramid level
	 * @return Pointer to data array
	 */
	double* getLevelData (int l) {
		return levels[l];
	}

	/**
	 * @brief Get pyramid level width 
	 * @param l Desired pyramid level
	 * @return Pyramid level width
	 */
	int getLevelWidth (int l) {
		return levelDimensions[l][0];
	}

	/**
	 * @brief Get pyramid level height 
	 * @param l Desired pyramid level
	 * @return Pyramid level height
	 */
	int getLevelHeight (int l) {
		return levelDimensions[l][1];
	}

	/**
	 * @brief Get number of channels in pyramid's images
	 * @return Number of channels
	 */
	int getSpectrum () {
		return channels;
	}

	/**
	 * @brief Convert pyramid to Laplacian Pyramid
	 * @return Returns true if succeeded
	 */
	bool convertToLaplacian () {
		for (int i = 1; i < nlevels; ++i)
		{
			expandAndSubtractBelow(i);
		}
		return true;
	}

	/**
	 * @brief Collapse levels top-down into full resolution image
	 * @details Each layer n is expanded and summed to layer n-1, until it 
	 * reaches layer 0
	 * @return Returns true if succeeded
	 */
	bool collapsePyramid () {
		for (int i = (nlevels-1); i > 0 ; --i)
		{
			collapseBelow(i);
		}
		return true;
	}

	/**
	 * @brief Blend this pyramid on top of pyramid p2, using mask pb
	 * @details [long description]
	 * 
	 * @param p2 Bottom pyramid
	 * @param blend Blending mask pyramid (considers only 1 channel)
	 * 
	 * @return Returns true if succeeded
	 */
	bool blendWith (ImagePyramid &p2, ImagePyramid &blend) {
		int pixel = 0;
		int w = 0;
		int h = 0;
		double ratio = 0.0;

		for (int l = 0; l < nlevels; ++l)
		{
			w = levelDimensions[l][0];
			h = levelDimensions[l][1];
			for (int c = 0; c < channels; ++c)
			{
				for (int y = 0; y < h; ++y)
				{
					for (int x = 0; x < w; ++x)
					{
						ratio = blend.get(l, x, y, 0)/255.0;
						pixel = x + y*w + c*w*h;
						levels[l][pixel] = ratio*levels[l][pixel] + (1.0-ratio)*p2.get(l, x, y, c);
					}
				}
			}
		}
	}

	/**
	 * @brief Get image value if it exists, return 0.0 otherwise
	 * @details [long description]
	 * 
	 * @param l Level of image
	 * @param x X coordinate of pixel on image
	 * @param y Y coordinate of pixel on image
	 * @param c Channel on pixel
	 * @return Returns value if in bounds and 0.0 otherwise
	 */
	double get(int l, int x, int y, int c) {
		if(l >= nlevels || l < 0 || x < 0 || x >= width || y < 0 || y >= height || c < 0 || c >= channels)
			return 0.0;
		int w = levelDimensions[l][0];
		int h = levelDimensions[l][1];
		int pixel = x + y*w + c*w*h;
		return levels[l][pixel];
	}

	/**
	 * @brief Destructor
	 */
	~ImagePyramid() {
		for (int i = 0; i < nlevels; ++i)
		{
			delete [] levels[i];
			delete [] levelDimensions[i];
		}
		delete [] levels;
		delete [] levelDimensions;
		if (temp != 0)
		{
			delete [] temp;
		}
	}


private:

	//array containing arrays with pyramid levels' data
	double **levels;
	//array containing level dimensions, starting at level0
	int **levelDimensions;
	//temporary image
	double *temp;
	//Gaussian Kernel
	double kernel[5][5]; //optimization: could be stored as a 2x2
	//Gaussian Kernel divider
	double divider;
	//number of levels
	int nlevels;
	//level 0 width
	int width;
	//level 0 height
	int height;
	//channels
	int channels;
	//if pyramid has been computed
	bool computed;

	/**
	 * @brief initialize class variables as an empty pyramid
	 * @return Returns true if function succeded
	 */
	bool initialize () {
		levels = 0;
		levelDimensions = 0;
		temp = 0;
		divider = 0.0;
		width = 0;
		height = 0;
		nlevels = 0;
		channels = 0;
		computed = false;
		return true;
	}

	/**
	 * @brief Set the number of pyramid levels
	 * @param nlev Number of levels
	 * @return Returns true if function succeded
	 */
	bool setNumberOfLevels (int nlev) {
		nlevels = nlev;
		return true;
	}

	/**
	 * @brief Sets the level0 dimensions and # of channels from the original image
	 * @param w Width of level0 image
	 * @param h Height of level0 image
	 * @param c Number of channels on image
	 * @return Returns true if function succeded
	 */
	bool setDimensions (int w, int h, int c) {
		width = w;
		height = h;
		channels = c;
		return true;
	}

	/**
	 * @brief Create pyramid array of arrays to store data, if it's not created yet
	 * @return Returns true if function succeded
	 */
	bool createArrays () {
		if (!levels)
		{
			levels = new double*[nlevels];
			levelDimensions = new int*[nlevels];
			int w = width;
			int h = height;
			for (int i = 0; i < nlevels; ++i)
			{
				levelDimensions[i] = new int[2];
				levelDimensions[i][0] = w;
				levelDimensions[i][1] = h;
				levels[i] = new double[w*h*channels];
				w /= 2;
				h /= 2;
			}
			return true;
		}
		return false;
	}

	/**
	 * @brief Create pyramid array of arrays to store data, if it's not created yet. Fill with data
	 * @param data Data to fill array
	 * @return Returns true if function succeded
	 */
	bool createArraysAndFill (const double *data) {
		if (!levels)
		{
			levels = new double*[nlevels];
			levelDimensions = new int*[nlevels];
			int w = width;
			int h = height;
			for (int i = 0; i < nlevels; ++i)
			{
				levelDimensions[i] = new int[2];
				levelDimensions[i][0] = w;
				levelDimensions[i][1] = h;
				levels[i] = new double[w*h*channels];
				w /= 2;
				h /= 2;
			}

			//fill level 0
			int pixels = width*height*channels;
			for (int i = 0; i < pixels; ++i)
			{
				levels[0][i] = data[i];
				truncate0_255(levels[0][i]);
			}
			return true;
		}
		return false;
	}

	/**
	 * @brief Create pyramid array of arrays to store data, if it's not created yet. 
	 * Fill with data normalized to 0.0,1.0 interval
	 * 
	 * @param data Data to fill array
	 * @return Returns true if function succeded
	 */
	bool createArraysAndFillNormalized (const double *data) {
		if (!levels)
		{
			levels = new double*[nlevels];
			levelDimensions = new int*[nlevels];
			int w = width;
			int h = height;
			for (int i = 0; i < nlevels; ++i)
			{
				levelDimensions[i] = new int[2];
				levelDimensions[i][0] = w;
				levelDimensions[i][1] = h;
				levels[i] = new double[w*h*channels];
				w /= 2;
				h /= 2;
			}

			//fill level 0
			int pixels = width*height*channels;
			for (int i = 0; i < pixels; ++i)
			{
				levels[0][i] = data[i];
			}
			return true;
		}
		return false;
	}

	/**
	 * @brief Sets up the Gaussian filter kernel
	 * @param dZero Gaussian filter Cutoff Frequency
	 * @return Returns true if function succeeded
	 */
	bool computeKernel (double dZero) {
		divider = 0.0;
		double temp = 0.0;
		for (int u = -2; u < 3; ++u)
		{
			for (int v = -2; v < 3; ++v)
			{
				temp = std::exp( -( ( (u*u) + (v*v) ) / (2*dZero*dZero) ) );
				kernel[u+2][v+2] = temp;
				divider += temp;
			}
		}
		return true;
	}

	/**
	 * @brief Sets up Gaussian filter kernel, based on integer approximation
	 * @return Returns true if function succeeded
	 */
	bool computeKernel () {

		kernel[0][0] = 1.0; kernel[0][1] = 4.0; kernel[0][2] = 6.0; kernel[0][3] = 4.0; kernel[0][4] = 1.0;
		kernel[1][0] = 4.0; kernel[1][1] = 16.0; kernel[1][2] = 24.0; kernel[1][3] = 16.0; kernel[1][4] = 4.0;
		kernel[2][0] = 6.0; kernel[2][1] = 24.0; kernel[2][2] = 36.0; kernel[2][3] = 24.0; kernel[2][4] = 6.0;
		kernel[3][0] = 4.0; kernel[3][1] = 16.0; kernel[3][2] = 24.0; kernel[3][3] = 16.0; kernel[3][4] = 4.0;
		kernel[4][0] = 1.0; kernel[4][1] = 4.0; kernel[4][2] = 6.0; kernel[4][3] = 4.0; kernel[4][4] = 1.0;
		divider = 256.0;
		
		return true;
	}

	/**
	 * @brief Computes all levels of the Gaussian pyramid
	 * @param data Original image array
	 * @return Returns true if function succeded
	 */
	bool privateComputeNonInterleaved () {
		int w = 0;
		int h = 0;
		int pixel = 0;
		double n00;
		for (int l = 1; l < (nlevels); ++l)
		{
			for (int c = 0; c < channels; ++c)
			{
				w = levelDimensions[l][0];
				h = levelDimensions[l][1];
				for (int x = 0; x < w; ++x)
				{
					for (int y = 0; y < h; ++y)
					{
						pixel = x + y*w + c*h*w;
						n00 = applyKernel(l-1, (x*2), (y*2), c);
						truncate0_255(n00);
						levels[l][pixel] = n00;
					}
				}
			}
		}
		return true;
	}

	/**
	 * @brief Expand level and subtract it from imediat inferior level
	 * 
	 * @param l Desired level to expand
	 * @return Returns true if succeeded
	 */
	bool expandAndSubtractBelow (int l) {
		if ((l-1)<0 || (l-1)>=nlevels)
		{
			return false;
		}
		int loww = levelDimensions[l-1][0];
		int lowh = levelDimensions[l-1][1];

		double *t = new double[loww*lowh*channels];

		int highw = levelDimensions[l][0];
		int highh = levelDimensions[l][1];
		int nx = 0; int ny = 0;
		int lowpixel=0; int highpixel = 0;
		for (int c = 0; c < channels; ++c)
		{
			for (int y = 0; y < lowh; ++y)
			{
				for (int x = 0; x < loww; ++x)
				{
					nx = x/2;
					ny = y/2;
					nx = (nx >= highw) ? 2*highw-nx-1 : nx;
					ny = (ny >= highh) ? 2*highh-ny-1 : ny;
					lowpixel = x + y*loww + c*loww*lowh;
					highpixel = nx + ny*highw + c*highw*highh;
					t[lowpixel] = levels[l][highpixel];
				}
			}
		}

		/**/
		double value = 0.0;
		for (int c = 0; c < channels; ++c)
		{
			for (int x = 0; x < loww; ++x)
			{
				for (int y = 0; y < lowh; ++y)
				{
					lowpixel = x + y*loww + c*lowh*loww;
					value = applyKernelTemp(t, x, y, c, loww, lowh, channels);
					levels[l-1][lowpixel] -= value;
				}
			}
		}
		/**/
		delete [] t;
		return true;
	}

	/**
	 * @brief Collapse expanded upper level on imediate lower level
	 * @details Expand level l and sum it to level l-1
	 * 
	 * @param l Desired level to expand and collapse below
	 * @return Returns true if succeeded
	 */
	bool collapseBelow (int l) {
		if ((l-1)<0 || (l-1)>=nlevels)
		{
			return false;
		}
		int loww = levelDimensions[l-1][0];
		int lowh = levelDimensions[l-1][1];

		double *t = new double[loww*lowh*channels];

		int highw = levelDimensions[l][0];
		int highh = levelDimensions[l][1];
		int nx = 0; int ny = 0;
		int lowpixel=0; int highpixel = 0;
		for (int c = 0; c < channels; ++c)
		{
			for (int y = 0; y < lowh; ++y)
			{
				for (int x = 0; x < loww; ++x)
				{
					nx = x/2;
					ny = y/2;
					nx = (nx >= highw) ? 2*highw-nx-1 : nx;
					ny = (ny >= highh) ? 2*highh-ny-1 : ny;
					lowpixel = x + y*loww + c*loww*lowh;
					highpixel = nx + ny*highw + c*highw*highh;
					t[lowpixel] = levels[l][highpixel];
				}
			}
		}

		/**/
		double value = 0.0;
		for (int c = 0; c < channels; ++c)
		{
			for (int x = 0; x < loww; ++x)
			{
				for (int y = 0; y < lowh; ++y)
				{
					lowpixel = x + y*loww + c*lowh*loww;
					value = applyKernelTemp(t, x, y, c, loww, lowh, channels);
					levels[l-1][lowpixel] += value;
					truncate0_255(levels[l-1][lowpixel]);
				}
			}
		}

		delete [] t;
		return true;	
	}

	/**
	 * @brief Apply the kernel defined in the class 'double kernel[5][5]' on array position
	 * @details [long description]
	 * 
	 * @param t Array representing image (non-interleaved)
	 * @param x X coordinate of pixel on image
	 * @param y Y coordinate of pixel on image
	 * @param c Channel on pixel
	 * @param dimX Width of image
	 * @param dimY Height of image
	 * @param spec Spectrum (number of channels) of image
	 * @return Returns result of kernel application
	 */
	double applyKernelTemp (double *t, int x, int y, int c, int dimX, int dimY, int spec) {
		if (c >= spec) return 0.0;
		double result = 0.0;
		int w = dimX;
		int h = dimY;
		/**/
		int pixel = 0;
		int nx = 0;
		int ny = 0;
		for (int u = -2; u < 3; ++u)
		{
			for (int v = -2; v < 3; ++v)
			{
				nx = ((x+u) < 0) ? (-(u+1)) : x+u;
				nx = (nx >= w) ? 2*w-nx-1 : nx;
				ny = ((y+v) < 0) ? (-(v+1)) : y+v;
				ny = (ny >= h) ? 2*h-ny-1 : ny;
				pixel = nx + ny*w + c*h*w;
				result += kernel[u+2][v+2]*t[pixel];
			}
		}
		result = result/divider;
		/**/
		return result;
	}

	/**
	 * @brief Apply the kernel defined in the class 'double kernel[5][5]'
	 * @param l Level to which we apply the kernel
	 * @param x X coordinate of target pixel
	 * @param y Y coordinate of target pixel
	 * @param c Channel in which we apply the kernel
	 * @return Final value for applied kernel
	 */
	double applyKernel (int l, int x, int y, int c) {
		
		double result = 0.0;
		int w = levelDimensions[l][0];
		int h = levelDimensions[l][1];
		/**/
		int pixel = 0;
		int nx = 0;
		int ny = 0;
		
		for (int u = -2; u < 3; ++u)
		{
			for (int v = -2; v < 3; ++v)
			{
				
				nx = ((x+u) < 0) ? (-(u+1)) : x+u;
				nx = (nx >= w) ? 2*w-nx-1 : nx;
				ny = ((y+v) < 0) ? (-(v+1)) : y+v;
				ny = (ny >= h) ? 2*h-ny-1 : ny;
				
				pixel = nx + ny*w + c*h*w;
				
				result += kernel[u+2][v+2]*levels[l][pixel];
				
			}
		}
		result = result/divider;
		
		/**/
		return result;
	}

	/**
	 * @brief Debug function to fill arrays with value
	 * @param v Filling value
	 * @return Returns true id function succeeded
	 */
	bool fillArrays (double v) {
		int w = 0;
		int h = 0;
		int pixel = 0;
		for (int l = 0; l < nlevels; ++l)
		{
			for (int c = 0; c < channels; ++c)
			{
				w = levelDimensions[l][0];
				h = levelDimensions[l][1];
				for (int x = 0; x < w; ++x)
				{
					for (int y = 0; y < h; ++y)
					{
						pixel = x + y*h + c*h*w;
						levels[l][pixel] = v;
					}
				}
			}
		}
		printLevels();
		return true;
	}

	/**
	 * @brief Debug function to print all data on all pyramid levels
	 */
	void printLevels () {
		for (int i = 0; i < nlevels; ++i)
		{
			std::cout<<"\n Level "<<i<<": \n";
			int dim = levelDimensions[i][0]*levelDimensions[i][1];
			for (int j = 0; j < dim; ++j)
			{
				std::cout<<levels[i][j]<<" ";
			}
			std::cout<<std::endl;
		}
	}

	/**
	 * @brief Truncate value between 0.0 and 255.0
	 * 
	 * @param d Value to be truncated
	 */
	inline void truncate0_255 (double &d) {
		d = (d > 255.0) ? 255.0 : d;
		d = (d < 0.0) ? 0.0 : d;
	}


};