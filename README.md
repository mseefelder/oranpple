# Laplacian Pyramid Blending

Terminal application to blend two images, using a mask, with Laplacian Pyramid Blending technique.

# References:

1. [COC603 - Processamento de Imagens (Image Processing)](http://www.lcg.ufrj.br/~marroquim/courses/cos756/index.php) - [UFRJ](http://www.ufrj.br/): [Trabalho 1 (Assignment 1)](http://www.lcg.ufrj.br/~marroquim/courses/cos756/assignments/assignment-lp-blending.pdf);
2. P. Burt and E. Adelson.  The laplacian pyramid as a compact image code. IEEE Transactions on Communications, 31(4):532–540, Apr 1983.
3. Peter J. Burt and Edward H. Adelson.  A multiresolution spline with application to imagemosaics. ACM Trans. Graph., 2(4):217–236, October 1983.

# BUILD INSTRUCTIONS

Download [CImg](http://www.cimg.eu/). You just need the header CImg.h
Copy CImg.h to project root folder

### Linux
On terminal, compile it like this:

	g++ main.cpp -o programName -lX11 -pthread

What are these flags?

* -lX11 includes Xlib, usually packaged with Linux
* -pthread includes pthread (Posix Threads) library, usually packaged with Linux

Both are included by CImg.

**Any problems with compilation contact me.**

# Usage instructions:

	Usage is: 
 	<image 1 path> <image 2 path> <blending mask path>
	The blending image should be a black and white image


# Some [results](http://www.lcg.ufrj.br/~mseefelder/oranpple)

